from flask import request, Blueprint
from scripts.handler.colorfinderhandler import print_nested
import json

color_finder = Blueprint("color-finder-blueprint", __name__)


@color_finder.route('/', methods=['POST'])
def postjsonhandler():
    content = request.get_json()
    # print(content)
    solution = print_nested(content)
    return solution
