uni_color = []


def print_nested(data):
    if not isinstance(data, str):
        for k, v in data.items():
            if isinstance(v, dict):
                print_nested(v)
            elif hasattr(v, '__iter__') and not isinstance(v, str):
                for item in v:
                    print_nested(item)
            elif isinstance(v, str):
                if k == "color":
                    uni_color.append(v)
            else:
                if k == "color":
                    uni_color.append(v)
    return str(set(uni_color))
