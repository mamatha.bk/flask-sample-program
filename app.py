from flask import Flask
from scripts.service.colorfinderservice import color_finder

app = Flask(__name__)
app.register_blueprint(color_finder)
if __name__ == '__main__':
    app.run()
